package sandbox

import cats.Foldable
import cats.instances.list._

object FoldableAndTraverse {
	val ints = List(1, 2, 3);
  
  Foldable[List].foldLeft(ints, 0)(_ + _);
  println("done")
}