package sandbox


/**
import cats.instances.string._
import cats.syntax.semigroup._
import cats.Foldable
**/
import cats.Foldable;
import cats.instances.list._;
  
object Main2 {
 // println("Hello " |+| "Cats!")
  List(1, 2, 3).foldLeft(0)(_ - _)
  
  // Foldable
 	List(1, 2, 3).foldLeft(List.empty[Int])((a, i) => i :: a)
 	
 	// will throw an error due to nil
  //List(1, 2, 3).foldLeft(Nil)((a, i) => i :: a)
 	
  /* Scaffolding other methods with foldLeft
  def map[A, B](list: List[A])(func: A => B):List[B] =
  	list.foldRight(List.empty[B]) { (item, acc) =>
  		func(item) :: acc
  	}
  */
  /**
  cats provides foldable instances for List, Vector, Stream and Option
  **/

  
 
 
 /**
   // Option
    import cats.instances.option._;
  val maybeInt = Option(123)
  Foldable[Option].foldLeft(maybeInt, 10)(_ * _)
 */
}