package StateMonad

import cats.data.State

/**
  * State[S, A] a fn of type S => (S, A)
  */
object StateMonadCats {
  val a = State[Int, String] { state =>
    (state, s"the state is $state")
  }
}

