import cats.data.State

/**
  * transforms an input state to an output state;
  * computers a result
  */
object StateMonadCats {
  val a = State[Int, String] { state =>
    (state, s"the state is $state")
  }

  // get the state and res
  val (state, result) = a.run(10).value

  // get the state only
  val state2 = a.runS(10).value

  val resul2 = a.runA(10).value

  // composing and transforming state
  val step1 = State[Int, String]  { num =>
    val ans = num + 1
    (ans, s"Result of step1 is $ans")
  }

  val step2 = State[Int, String] { num =>
    val ans = num * 2
    (ans, s"result of step2 is $ans")
  }

  val both = for {
    a <- step1
    b <- step2
  } yield(a,b)

  both.run(20).value

  /**
    * get - xtracts the state as the result
    * set - updates teh state and returns unit
    * pure - ignores the state and returns a supplied result
    * inspect - extracts the state via tx fn
    * modify  -  updates the state using update fn
    */

  type CalcState[A] = State[List[Int], A]

  def evalOne(sym: String): CalcState[Int] =
    sym match {
      case "+" => operator(_ + _)
    }

  State[List[Int], Int] { oldStack =>
    val newStack = someTrans(oldStack)
    val result = someCalc
    (newStack, result)
  }
}