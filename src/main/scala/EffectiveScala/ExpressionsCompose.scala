//package EffectiveScala
//
//import cats._
///**
//  */
//object ExpressionsCompose {
//
//  trait HttpPage;
//  def prepareHttpPage() = ???
//
//  def timeoutCatch: Any = {
//    case _ => true
//  }
//  def noPageCatch() = {
//    case _ => ???
//  }
//
//  def orElse(x: Any)(y: Any): Any = true
//
//  def home(): HttpPage =
//    try prepareHttpPage()
//   // catch timeoutCatch orElse noPageCatch
//}
//
///**
//  * return is overrated
//  *  - superflous, avoid multiple exit point, may impact perf
//  *  scalac A.scala -Xprint:uncurry
//  *
//  *  - don't use null, use option
//  */
//trait HttpSession
//
//object Auth {
//
//  def canAuth(user: String, pass: String) = ???
//  def inject(x: Any, y: Any) = ???
//  def privilegesFor(str: String) = ???
//  def authenticate(session: HttpSession,
//                   username: Option[String],
//                   password: Option[String]) = ???
////    for {
////      user <- username
////      pass <- password
////      if canAuth(user, pass)
////        privileges <- privilegesFor(user)
////    } yield inject(session, privileges)
//}
//
///**
//  * don't use exceptions for control flow
//  * - exceptions are for unrecoverable errors
//  * - create proper types for expected application errors
//  *
//  *
//  * # Stay immutable
//  * 3 reasons - simplifies reasoning
//  * - allows co/contra variance
//  * - mutablity + variance = troubles
//  *
//  * - immutablity is thread safe by design
//  * - mutablity is still ok, but keep it in local scopes
//  *
//  *
//  * Traversable(foreach) different to iterables
//  *  - in traversable u r not in control of iteration, ur collection is
//  *  - In Iterables u r in control
//  */