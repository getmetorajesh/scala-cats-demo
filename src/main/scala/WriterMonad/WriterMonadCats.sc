import cats.data.Writer
import cats.syntax.writer._
import cats.syntax.applicative._
import cats.instances.vector._

type Logged[A] = Writer[Vector[String], A]

/**
  * if u only have a value and no log
  */
123.pure[Logged]
43.pure[Logged]

/**
  * If u only have a log and  no value
  */
Vector("22", "22").tell

// if both

val a = Writer(Vector("mess1", "mess2"), 232)
// to get both values from writer
a.run

// to get only result
a.value

// to get only log
a.written

val writer1 = for {
  a <- 10.pure[Logged]
  _ <- Vector("a","b").tell
  _ <- 110.writer(Vector("cc"))
  _ <- 111.pure[Logged]
  b <- 32.writer(Vector("x", "z"))
} yield a + b

writer1.run

// map logs
writer1.mapWritten(_.map(_.toUpperCase()))

// map both logs and result using biMap or mapBoth
val writer2 = writer1.bimap(
  log => log.map(_.toUpperCase),
  res => res * 100
)
writer2.run

val writer3 = writer1.mapBoth {(log, res) =>
  val log2 = log.map(_ + "!")
  val res2 = res * 100
  (log2, res2)
}

writer3.run

/// factorial example

def slowly[A](body: => A) = try body finally Thread.sleep(100)

def factorial(n: Int): Int = {
  val ans = slowly(if(n == 0) 1 else n*factorial(n-1))
  println(s"fact of $n $ans")
  ans
}

factorial(5)

type factLogger[A] =  Writer[Vector[String], A]
def factorial2(n: Int) = {
  val ans = slowly(if(n == 0) 1 else n*factorial(n-1))
  n.writer(Vector(s"fact of $n $ans"))
}

factorial2(5).run

factorial2(5).written

def factorial5(n: Int): Logged[Int] =
  for {
    ans <- if(n == 0) {
      1.pure[Logged]
    } else {
      slowly(factorial5(n - 1).map(_ * n))
    }
    _ <- Vector(s"fact $n $ans").tell
  } yield ans

factorial5(5).run