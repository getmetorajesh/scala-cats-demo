package WriterMonad

import cats.data.Writer
import cats.instances.vector._
import cats.syntax.applicative._
import cats.syntax.writer._

/**
  */
object WriterMonadCats extends App {
  Writer(Vector("best", "worst"), 1859)

  type Logged[A] = Writer[Vector[String], A]

  123.pure[Logged]
}
