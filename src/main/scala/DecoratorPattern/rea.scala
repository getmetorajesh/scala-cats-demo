//package DecoratorPattern
//
//import cats.effect.Async
//
///**
//  */
//trait helper {
//  def toJson = {}
//}
//case class RawListing() extends helper
//case class FullListing() extends helper
//class ListingProcessor(transform: RawListing => FullListing,
//                       save: String => Async[Unit]) {
//  def process(listings: Seq[RawListing]): Async[Unit] = {
//    val fullListings = listings.map(transform)
//    //.. logic
//    save(fullListings)
//  }
//}
//
//// A place to put things. if it needs to retain data
//// we can name it to "Store". we happen to have async return value.
//trait Sink[A] {
//  def apply(a: A): Async[Unit]
//}
//
//class ListingProcessor2(transform:RawListing => FullListing,
//                        save: Sink[Seq[FullListing]]) {
// def process(listings: Seq[RawListing]): Async[Unit] = {
//    val fullListings = listings.map(transform)
//    //.. logic
//    save(fullListings)
//  }
//}
//
//
////class JsonSink[-A](internalSink: Sink[String])(implicit encode: Encoder[A]) extends Sink[A] {
////  override def apply(a:A) : Async[Unit] = {
////    internalSink(encode(a).noSpaces)
////  }
////}
////
////class HttpSink(http: HttpService) extends Sink[String] {
////  def apply(requestBody: String): Async[Unit] = {
////    http.put(requestBody).map(_ => ())
////  }
////}
////
////val httpSink = new HttpSink(HttpService(s"${config.apiHost}/v1/listings"))
////val jsonSink = new JsonSink[Seq[FullListing]](httpSink)
////val listingTransformer = //..
////new ListingProcessor2(listingTransformer, jsonSink)