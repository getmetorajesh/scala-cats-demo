import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClientBuilder
import com.amazonaws.services.sns.model.{PublishRequest, SetSMSAttributesRequest}

import collection.JavaConverters._

val profile = new ProfileCredentialsProvider("rajaws_techiepandas")
val s3Client = AmazonS3ClientBuilder.standard()
  .withCredentials(profile)
  .build()
val snsClient = AmazonSNSClientBuilder.standard()
                  .withCredentials(profile)
                .build()
  s3Client.listBuckets().asScala.map(i => println(i))

val setRequest = new SetSMSAttributesRequest()
  .addAttributesEntry("DefaultSenderID", "mySenderID")
  .addAttributesEntry("MonthlySpendLimit", "1")
  .addAttributesEntry("Message", "10")
  .addAttributesEntry("DefaultSMSType", "Promotional")
snsClient.setSMSAttributes(setRequest)

val message = "My SMS message"
val phoneNumber = "+"


val publishRes = snsClient.publish(new PublishRequest()
.withMessage(message)
    .withPhoneNumber(phoneNumber)
)

println(publishRes)