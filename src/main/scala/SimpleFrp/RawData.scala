package SimpleFrp

/**
  */
case class RawUser(fullName: String,
                   email: String,
                   phone: String,
                   street: String,
                   city: String,
                   zipCode: String) {
  lazy val person = ???
}
object RawData {

  def generateRawUsers: Seq[RawUser] = Iterator(
    RawUser("r", "r@r.com", "73388993","","","")
  ).toSeq

}
