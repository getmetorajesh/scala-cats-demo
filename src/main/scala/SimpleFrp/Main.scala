package SimpleFrp

/**
  */
object Main {

  private def go(emit: () => Seq[RawUser]): Result = ???
  private def transform(r: RawUser): DomainUser = ???
  private def toResult(v: DomainUser): Result = ???

}

case class Result(successes: Int, failure: Int)
case class PhoneNumber(countryCode: Int,
                       lineNumber: Int)
case class Person()
case class DomainUser(person: Person,
                      phoneNumber: PhoneNumber)

//
//object PhoneNumber {
//  val pattern = s"""(\d{1}-\d{2})""".r
//  def from(phoneString: String): TransformError \/ PhoneNumber = {
//    phoneString match {
//      case pattern(first, second) => ???
//      case _ => -\/ TransformError(s"Phone string didn't parse")
//
//    }
//
//  }
//}