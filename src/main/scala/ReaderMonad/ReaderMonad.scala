package ReaderMonad

import cats.data.Reader
/**
  * Reader monad represents a fn A => B
  * class Reader[A,B](run: A => B){}
  * used to achive composition and DI
  */
object ReaderMonad extends App {
  val upper = Reader((text: String) => text.toUpperCase())
  val greet = Reader((name: String) => s"Hello $name")

  // 2 readers with same type so compose
  val combo1 = upper.compose(greet)
  val combo2 = upper andThen greet

  val result = combo1.run("raj")
}

/**
  * Reader monad can be used for Di as well
  *
  */
object ReaderDi {

  case class Course(desc: String, code: String)

  class AuthService {
    def isAuthorised(username: String): Boolean = username.startsWith("s")
  }

  class CourseService {
    def register(course: Course, isAuthorised: Boolean, name: String) = {
      if (isAuthorised)
        s"User $name registered for the course: ${course.code}"
      else
        s"User: $name is not authorised to register for course: ${course.code}"
    }
  }

  case class CourseManager(course: Course,
                           userName: String,
                           authService: AuthService,
                           courseService: CourseService)

  def isAuthorised = Reader[CourseManager, Boolean] { courseMgr => {
    courseMgr.authService.isAuthorised(courseMgr.userName)
   }
  }

  def register(isFull: Boolean) = Reader[CourseManager, String] { courseMgr => {
    courseMgr.courseService.register(courseMgr.course, isFull, courseMgr.userName)
  }}
}