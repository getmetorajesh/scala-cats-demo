package ReaderMonad

import cats.data.Reader
import cats.syntax.applicative._ // for pure
/**
  */
object DBReaderProblem extends App {
//  case class SomeDb(names: String)

  case class Db(
    usernames: Map[Int, String],
    passwords: Map[String, String])
  val d = new  Db(Map(1 -> "ss"), Map("ss" -> "SSP"))
  type DbReader[A] = Reader[Db, A]

  def findUsername(userId: Int): DbReader[Option[String]] = Reader { db =>
    db.usernames.get(userId)

  }

  def checkPassword(username: String, password: String): DbReader[Boolean] = Reader { db =>
    db.passwords.get(username).contains(password)
  }

  def checkLogin(userId: Int, password: String): DbReader[Boolean] = {
    for {
      u <- findUsername(userId)
      p <- u.map {
          u => checkPassword(u, password)
        }.getOrElse {
         false.pure[DbReader]
      }
    } yield p
  }

  val passwords = Map(
    "dade" -> "zerocool",
    "kate" -> "acidburn",
    "margo" -> "secret")
  val users = Map(
    1 -> "dade",
    2 -> "kate",
    3 -> "margo"
  )

  val db = Db(users, passwords)

  val res = checkLogin(2, "acidburn").run(db)

  println(res)

}
