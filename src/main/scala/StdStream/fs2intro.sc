import cats.Eval
import cats.effect.IO
import cats.kernel.Semigroup
import fs2._


val s = Stream(1,2, 3)
  .map(_ + 1)
  .map(_.toString())
//  .toList

val t = Stream(10,14).flatMap { n => Stream.emits(List.fill(n)(n)) }

t.toList
t.toVector

(s interleave t).toList
(s intersperse 42).toList

val range = Stream.range(0, 10)
val range2 = Stream.range(20, 30)

(range zip range2).toList
//(range zip range2.repeat).toList

/**
  * IO
  */
val t1= Eval.now(10)

//t1.map { i => i+1}

// delay
//val d = IO.Delay { println("computing ..."); System.currentTimeMillis }
//val dres = d.map { i => i +1 }
//dres.unsafeRunSync()


//val ec = scala.concurrent.ExecutionContext


val aMap = Map("foo" → Map("bar" → 5))
val anotherMap = Map("foo" → Map("bar" → 6))
val combinedMap = Semigroup[Map[String, Map[String, Int]]].combine(aMap, anotherMap)

println(combinedMap.get("foo"))



