package StdStream

/**
  */
object fs2io extends  App{
  import java.nio.file.Paths

  import cats.effect.IO
  import fs2.{io, text}

  val file  = "/Users/preethi/Desktop/thoughts.md"
  val pa = Paths.get(file)

  val src = io.file.readAll[IO](pa, 4096)


//  List(1,2,3).flatMap( i => List(i))
  //src.compile.toList.unsafeRunSync()
  src
    .through(text.utf8Decode)
    .through(text.lines)
    .map(x => println(x))
    .compile
    .drain
    .unsafeRunSync()

}


