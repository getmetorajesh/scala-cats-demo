//import cats.Eval
//import cats.effect.IO
//import cats.kernel.{Monoid, Semigroup}
//import fs2._
import cats.implicits._
//
//
//
////val ec = scala.concurrent.ExecutionContext
//
//type someMap = Map[String, Map[String, Int]]
//val aMap = Map("foo" → Map("bar" → 5))
//val anotherMap = Map("foo" → Map("bar" → 6))
////val combinedMap = Semigroup[Map[String, Map[String, Int]]].combine(aMap, anotherMap)
//val cm = aMap |+| anotherMap
//println(cm.get("foo"))
//
//Monoid[String].empty
//
//Monoid[Map[String, Int]].combineAll(List(Map("a" → 1, "b" → 2), Map("a" → 3)))
//
//Monoid[Map[String, Int]].combineAll(List())

val l = List(1, 2, 3, 4, 5)
l.foldMap(identity)
l.foldMap(i => i.toString())

l.foldMap(i ⇒ (i, i.toString))