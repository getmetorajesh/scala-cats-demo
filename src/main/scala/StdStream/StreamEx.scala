import cats.data.State
import cats.effect.{IO, Sync}
import fs2.{io, text}
import fs2._
// import java.nio.file.Paths

object StreamEx extends App {

  io.stdin[IO](4096)
    .through(text.utf8Decode)
    .through(text.lines)
    .map(x => {
      println(x)
      x
    })
    .compile.drain

//    .collect { case Some(cmd) => cmd }
//    .runFold(State.pure[Simulation, Unit](())) { (z, cmd) => z.flatMap(_ => cmd.run) }
    .unsafeRunSync()
}