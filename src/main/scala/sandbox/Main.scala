package sandbox


import cats.data.State
//import cats.instances.list._

object Main extends App { 

 // println("Hello " |+| "Cats!") 

	val ints = List(1, 2, 3);

  val RobotState = State[Int, String] { state =>
    (state, s"state is $state")
  }
  List(1, 2,3,4,5).map(i => RobotState.runS(i).value)
}