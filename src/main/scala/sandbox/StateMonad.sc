// Cats State allows us to pass additional state around as part of the computation

import cats.data.State

val a = State[Int, String] { state =>
  (state, s"state is $state")
}

/* an instance of state is a fn that does 2 things
1. transforms an input state to an o/p state
2. computes a result

run a monad supplying an initial state
State provides 2 methods run, runA, runS that returns diff comb of state
each returns an instance of Eval, which State uses to maintain stack safety
we can value to extract the actual result
*/
val (state, result) = a.run(10).value

println(state)

// only state
val onlyState = a.runS(15).value


a.run(10)
// only result
val onlyResult = a.runA(19).value
case class Robot(id: Int)

type RobotState[A] = State[Robot, A]


val x = State.set[Robot](Robot(10))



def move:RobotState[Unit] = State.set[Robot](Robot(199))

def report:RobotState[Unit] = State.get[Robot].map(x => x)

println(move.run(Robot(2)).value)
move.run(Robot(33))
move.get.map(x => println(x.id))
//println(report.run(Robot(333)).value)


final case class Seed(long: Long) {
  def next = Seed(long + 6 + 1)
}
val nextLong: State[Seed, Long] = State(seed =>{
  println(seed.long)
  println(seed.next)
  (seed.next, seed.long)
}
)


//val nextBoolean:State[Seed, Boolean] = nextLong.map(long => long > 0)

//println(nextBoolean.runS(Seed(2L)).value)


nextLong.run(Seed(1L)).value

nextLong.run(Seed(2L)).value

List(1L, 2L).map(i => nextLong.run(Seed(i)).value)

type Stack = List[Int]
def stackyStack: State[Stack, Unit] = for {
  stackNow <- State.get[Stack]
  r <- if (stackNow == List(1, 2, 3)) State.set[Stack](List(8, 3, 1))
  else State.set[Stack](List(9, 2, 1))
} yield r

stackyStack.run(List(1,2,3)).value

stackyStack.run(List(1,2,3)).value
