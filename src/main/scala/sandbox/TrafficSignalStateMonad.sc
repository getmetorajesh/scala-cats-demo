sealed trait Aspect
case object Green extends Aspect
case object Amber extends Aspect
case object Red   extends Aspect

sealed trait Mode
case object Off      extends Mode
case object Flashing extends Mode
case object Solid    extends Mode

// represents the actual display set: must be enabled before
// it can be used.
case class Signal(
                   isOperational: Boolean,
                   display: Map[Aspect, Mode])



  import cats.data.State
  import cats.kernel.Semigroup


  type SignalState[A] = State[Signal, A]

  val default = Signal(
    isOperational = true,
    display = Map(Red -> Flashing, Amber -> Off, Green -> Off)
  )

//  def enable: State[Signal, Boolean] = State { signal =>
//
//    for {
//      signal <- signal
//    } yield (signal, signal.isOperational)
//  }
//
//
//  enable.run(Signal(false, Map(Amber -> Off))).value


def addCount(n: Int): State[Int, String] = State { acc =>
  (acc, s"${acc+n} is the value")

}

addCount(2).run(4).value