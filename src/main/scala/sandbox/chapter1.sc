import java.util.Date

import cats.kernel.Semigroup
import cats.{Eq, Show}

implicit val dateShow: Show[Date] =
  Show.show(date => s"${date.getTime}ms")

1=="2"

import cats.syntax.eq._
// have to import int as well
import cats.instances.int._
import cats.instances.string._
123 === 123

//123 === "123" // will not work fair enough

import cats.instances.option._
import cats.instances.long._
//Some(1) === None
val d = new Date(11, 11, 2012)
implicit val dateEq: Eq[Date] =
  Eq.instance[Date] { (date1, date2) =>
    date1.getTime === date2.getTime
  }


/**
  * |@| cartesian builder
  */

import cats.implicits._

(5.some |@| 6.some).map(_ + _)
//(Some(5).get + Some(6).get)

def addOptions(a: Option[Int], b: Option[Int]) = (a |@| b).map(_ + _)
addOptions(3.some, None)

// Eg with future
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

def futureCo(a: Future[Int], b: Future[Int]) = (a |@| b).map(_+_)

futureCo(Future.successful(22), Future.successful(2))

//.foreach(println)

Option(2) |+| None

Map("a" -> 1) |+| Map("b" -> 2)

Semigroup[Int].combine(1,2)

Semigroup[List[Int]].combine(List(1, 2, 3), List(4, 5, 6))

Semigroup[Option[Int]].combine(Option(1), Option(2))

Semigroup[Option[Int]].combine(Option(1), None)