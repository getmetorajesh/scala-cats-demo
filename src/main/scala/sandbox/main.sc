//package sandbox


/**
import cats.instances.string._
import cats.syntax.semigroup._
import cats.Foldable
**/
import cats.Foldable;
import cats.instances.list._;

List(1, 2, 3).foldLeft(0)(_ - _)

// Foldable
List(1, 2, 3).foldLeft(List.empty[Int])((a, i) => i :: a)

// will throw an error due to nil
//List(1, 2, 3).foldLeft(Nil)((a, i) => i :: a)

/* Scaffolding other methods with foldLeft
def map[A, B](list: List[A])(func: A => B):List[B] =
  list.foldRight(List.empty[B]) { (item, acc) =>
    func(item) :: acc
  }
*/
/**
cats provides foldable instances for List, Vector, Stream and Option
  **/
// Option
import cats.instances.option._;
val maybeInt = Option(123)
val res = Foldable[Option].foldLeft(maybeInt, 10)(_ * _)
println(res)
//}


// default stream
import cats.Eval
import cats.instances.stream._
def bigData = ( 1 to 1000000).toStream
//bigData.foldRight(0L)(_ + _) // will cause overflow error

// using Eval means we are always stack safe
val eval = Foldable[Stream]
  .foldRight(bigData, Eval.now(0L)) {(num, eval) =>
    eval.map(_ + num)
  }

eval.value


/*
 find, exists, forAll, toList, isEmpty, nonEmpty
 combineAll(fold) combines all elements in seq using their monoid
  and foldMap map a seq and combines them.
  */
import cats.instances.int._
import cats.instances.string._
import cats.instances.vector._
Foldable[List].combineAll(List(1,2,3))
Foldable[List].foldMap(List(1,2,3))(_.toString)

// foldable to support deep traversal of nested strings
val ints = List(Vector(1,2,3), Vector(4,5,6))
(Foldable[List] compose Foldable[Vector]).combineAll(ints)

// Explicits over implicits

/**
  * Traverse
  */
