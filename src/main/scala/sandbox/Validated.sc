import cats.data.{NonEmptyList, ValidatedNel}
import cats.implicits._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Success, Try}
type ValidationResult[A] = ValidatedNel[String, A]

def validateCity(city: String): ValidationResult[String] = {
  city match {
    case "ODC" => city.validNel
    case _ => s"City in invalid not odc".invalidNel
  }
}

def convertToInt(axis: String): ValidationResult[Int] = {
  Try(axis.toInt) match {
    case Success(intVal) => return intVal.validNel
    case _ => return s"Unable to convert ${axis} to integer".invalidNel
  }
}



val validations1: List[ValidatedNel[String, Int]] = List(1.validNel[String], 2.valid, 3.valid)

(validateCity("ODC"), validateCity("O2DC")) mapN (Tuple2.apply)
//(1.validNel[String]) mapN (Tuple1.apply)

//val valList1: ValidatedNel[String, List[Int]] = validations1.sequence
//
//val s:ValidatedNel[String, List[Int]]  = validations1.combineAll



//convertToInt("ii").isInvalid
//
//case class CityCom(city: String, c2: String)
//
//(validateCity("OD"), validateCity("O2DC")) mapN (CityCom.apply)