import java.nio.file.Paths

import cats.effect.{IO, Sync}
import fs2.{io, text}


//io.file.readAll[IO](Paths.get("/Users/preethi/Sites/scala/cats-sandbox/resources/f.txt"), 4096)
////  .through(text.utf8Decode)
//  .through(commandParse)
//  .through(comamndHandler)
//  .through(text.lines)
//  .map( x => println(x + " sdad"))
//  .compile.drain
//  .unsafeRunSync()

val a::b::c = List(1,2,3)

println(a)
println(b)
println(c)

io.file.readAll[IO](Paths.get("/Users/preethi/Sites/scala/cats-sandbox/resources/f.txt"), 4096)
  .through(text.utf8Decode)
  .compile.drain
.run