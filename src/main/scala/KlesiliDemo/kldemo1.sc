def mul2: Int => Int = _ * 2

def power2: Int => Double = Math.pow(_, 2)

def doubleToInt: Double => Int = _.toInt

def intToString: Int => String = _.toString

val pipeline = power2 compose mul2

pipeline(2)

// Monadic functions can't do that. ie fn returning values
// inside a context

def stringToNonEmptyString: String => Option[String] = value => if(value.nonEmpty) Some(value) else None

def stringToNumber: String => Option[Int] = value =>
  if (value.matches("-?[0-9]+")) Option(value.toInt) else None

//stringToNumber compose stringToNonEmptyString


import cats.data.Kleisli
import cats.implicits._

val stringToNonEmptyStringK = Kleisli(stringToNonEmptyString)
val stringToNumberK = Kleisli(stringToNumber)

val pipek = stringToNumberK compose stringToNonEmptyStringK
pipek

case class cmd(c: String)
def validateInput(ip: String): Option[cmd] = {
  ip match {
    case "P" => Option(cmd(ip))
    case _ => None
  }
}

def processCmd(c: cmd): Option[String] = Option {
   "dsdsds"
}


def validateInputK = Kleisli(validateInput)
def processCmdK = Kleisli(processCmd)
val p = validateInputK andThen processCmdK
p("P")

val pipeline2: String => Option[Int] = Option(_) >>= stringToNonEmptyString >>= stringToNumber
pipeline2("232")


def countPositive(nums: List[Int]) =
  nums.foldLeft((0).asRight) { (accumulator, num) =>
    if(num > 0) {
      accumulator.map(_ + 1)
    } else {
      Left("Negative. Stopping!")
    }
  }

countPositive(List)