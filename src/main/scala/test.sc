import java.io.{File, PrintWriter}
import java.math.{BigDecimal, RoundingMode}

import scala.io.Source;


val visionCents = "0";
val DIVISOR = 100;


new BigDecimal(visionCents).divide(new BigDecimal(DIVISOR)).setScale(2, RoundingMode.FLOOR)
  //.setScale(2)


abstract class Animal

val l = Map("s" -> 1)
l.get("s")

List(1,1,1)


val writer = new PrintWriter(new File("Write.txt"))

writer.close()

l.foreach(l => writer.write(s"${l._1} ${l._2}\n"))
writer.close()
//def count(hostname: String, log: Map[String, Int]): Map[String, Int] = {
//   log.contains(hostname) match {
//     case true => log(hostname -> 1)
//   }
//}