package ModelFirstMS

import cats.data.EitherT

import scala.concurrent.Future
import cats.implicits._
import cats.data._
import cats._
import cats.syntax.functor._
import cats.instances.list._ // for Semigroupal
/**
  */
import scala.concurrent.ExecutionContext.Implicits.global
object ArticleSearchDomain {

  case class Author(id: Long, name: String)
  case class Publication(id: Long, authorId: Long, title: String)

  case class AuthorPublications(author: Author, publications: List[Publication])

  def findAuthor(query: String): Future[Long] = ???

  def getAuthor(id: Long): Future[Author] = ???

  def getPublications(authorId: Long): Future[List[Publication]] = ???

  def findPublications(query: String): Future[AuthorPublications] = 
  for {
    authorId <- findAuthor(query)
    author <- getAuthor(authorId)
    publications <- getPublications(authorId)
  } yield AuthorPublications(author, publications)

  val query = "dd"


  val search: Future[Unit] = findPublications(query) map { authroPubs =>
//    renderResponse(200, s"Found $authorPubs")
  }

//  Await.result(search, Duration.Inf)

  // Using cats parallel
  def findPublicationsC(query: String): Future[AuthorPublications] = ???
//  for {
//    authorId <- findAuthor(query)
//    (author, pubs) <- getAuthor(authorId) product getPublications(authorId)
//  } yield AuthorPublications(author, pubs)

  // exceptions should be avoided if its a domain error/problem
  // define domain error using sealed type hierarchies. sealed type 
  // hierarchies are a convenient way to encode failure values in scala.

  sealed trait ServiceError
  case object InvalidQuery extends ServiceError
  case object NotFound extends ServiceError
  // with scala 2.12 and cats Either is right biased.
  // i.e u can use map& flatMap to follow the happy path

  def findAuthorC(query: String) : Future[Either[ServiceError, Long]] =
  Future.successful(
    if (query == "vaugh") 42L.asRight
    else if (query.isEmpty) InvalidQuery.asLeft
    else NotFound.asLeft
  )

//  findAuthor("vv") map { idOrError => idOrError map { id => ...}}

  trait  SearchError
  // thats where monad transformers comes in to unwrap Future and Either 
  type Result[A] = EitherT[Future, SearchError, A]

}
