case class Author(id: Long, name: String)
case class Publication(id: Long, authorId: Long, title: String)

case class AuthorPublication(author: Author, publications: List[Publication])