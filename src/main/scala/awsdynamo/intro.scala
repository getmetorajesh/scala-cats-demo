//import com.gu.scanamo._
//import com.gu.scanamo.syntax._
//
//val client = LocalDynamoDB.client()
//
//import com.amazonaws.services.dynamodbv2.models.ScalaAttributeType._
//
//LocalDynamoDB.createTable(client)("muppets")('name -> S)
//
//case class Muppet(name: String, species: String)
//
//val muppets = Table[Muppet]("muppets")
//
//val ops = for {
//  _ <- muppets.put(Muppet("kermit", "frog"))
//  _ <- muppets.put(Muppet("cookie monster", "Monster"))
//  kermit <- muppets.get('name -> "Kermit")
//} yield kermit
//
//Scanamo.exec(client)(operations)
//
